<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** @ORM\Entity */
class Links {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

    /** @ORM\Column(type="string") */
    protected $admin_link;
    
    /** @ORM\Column(type="string") */
    protected $user_link;
    
    /** @ORM\Column(type="string") */
    protected $create_date;

    /** @ORM\Column(type="string") */
    protected $name;
    
    /** @ORM\Column(type="integer", options={"default" = 0}) */
    protected $vote;        
    
    /** @ORM\Column(type="integer", options={"default" = 0}) */
    protected $groupe;       
    
    public function __construct()
    {
        $this->vote = 0;
        $this->groupe = 0;
        $this->create_date = date("Y-m-d H:i:s");
    }
    
    // getters/setters
    function getId() {
        return $this->id;
    }
    
    function getVote() {
        return $this->vote;
    }
    function getGroupe() {
        return $this->groupe;
    }
    function getAdmin_link() {
        return $this->admin_link;
    }

    function getUser_link() {
        return $this->user_link;
    }

    function getCreate_date() {
        return $this->create_date;
    }

     function getName() {
            return $this->name;
     }

    function setVote($vote) {
        $this->vote = $vote;
    }
    
    function setGroupe($group) {
        $this->groupe = $group;
    }
    
    function setAdmin_link($admin_link) {
        $this->admin_link = $admin_link;
    }

    function setUser_link($user_link) {
        $this->user_link = $user_link;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setName($name) {
       $this->name = $name;
    }
    
}
