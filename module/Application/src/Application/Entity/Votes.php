<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** @ORM\Entity */
class Votes {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;
    /** @ORM\Column(type="integer") */
    protected $tab_id;
    /** @ORM\Column(type="integer") */
    protected $note_id;
    /** @ORM\Column(type="string") */
    protected $vote_date;

    public function __construct()
    {
        $this->vote_date = date("Y-m-d H:i:s");
    }
    
    // getters/setters
    function getId() {
        return $this->id;
    }
    function getTab_id() {
        return $this->tab_id;
    }
    function getNote_id() {
        return $this->note_id;
    }
    function getVote_date() {
        return $this->vote_date;
    }
    function setTab_id($tabId) {
        $this->tab_id = $tabId;
    }
    function setNote_id($noteId) {
        $this->note_id = $noteId;
    }
    function setVote_date($vote_date) {
        $this->vote_date = $vote_date;
    }

}