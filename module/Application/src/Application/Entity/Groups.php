<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** @ORM\Entity */
class Groups {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;
    /** @ORM\Column(type="integer") */
    protected $id_group;
    /** @ORM\Column(type="integer") */
    protected $tab_id;
    /** @ORM\Column(type="string") */
    protected $name;

    
    public function __construct()
    {
        ;
    }

    // getters/setters
    function getId() {
        return $this->id;
    }
    function getTab_id() {
        return $this->tab_id;
    }
    function getName() {
        return $this->name;
    }
    function getId_Group() {
        return $this->id_group;
    }
    function setTab_id($tabId) {
        $this->tab_id = $tabId;
    }
    function setName($name) {
        $this->name = $name;
    }
    function setGroupId($id_group) {
        return $this->id_group=$id_group;
    }

}