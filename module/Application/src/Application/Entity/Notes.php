<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** @ORM\Entity */
class Notes {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;
    /** @ORM\Column(type="integer") */
    protected $tab_id;
    /** @ORM\Column(type="string") */
    protected $name;
    /** @ORM\Column(type="string") */
    protected $description;
    /** @ORM\Column(type="text") */
    protected $create_date;
    /** @ORM\Column(type="integer", options={"default" = 0}) */
    protected $votes;
    /** @ORM\Column(type="integer") */
    protected $groupe;
    
    public function __construct()
    {
        $this->votes = 0;
        $this->groupe = 0;
    }

    // getters/setters
    function getId() {
        return $this->id;
    }
    function getTab_id() {
        return $this->tab_id;
    }
    function getName() {
        return $this->name;
    }
    function getDescription() {
        return $this->description;
    }
    function getCreate_date() {
        return $this->create_date;
    }
    function getVote() {
        return $this->votes;
    }
    function getGroup() {
        return $this->groupe;
    }
    function setTab_id($tabId) {
        $this->tab_id = $tabId;
    }
    function setName($name) {
        $this->name = $name;
    }
    function setDescription($description) {
        $this->description = $description;
    }
    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }
    function setVote($vote) {
        $this->votes = $vote;
    }
    function setGroup($group) {
        $this->groupe=$group;
    }

}