<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class VoteController extends AbstractActionController{
    
    public function startVoteAction(){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $links = $objectManager->getRepository('\Application\Entity\Links')
                ->findBy(array('id' => $this->getEvent()->getRouteMatch()->getParam('id')));
        $links[0]->setVote(1);
        
       $objectManager->persist($links[0]);
       $objectManager->flush();
       return false;
    }
    
    public function endVoteAction(){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $links = $objectManager->getRepository('\Application\Entity\Links')
                ->findBy(array('id' => $this->getEvent()->getRouteMatch()->getParam('id')));
        $links[0]->setVote(0);
        
       $objectManager->persist($links[0]);
       $objectManager->flush();
       return false;
    }
    
    public function addVoteAction(){
        
        if($this->getRequest()->getHeaders()->get('Cookie')->votesavailable != 0){
            $objectManager = $this->getServiceLocator()
                    ->get('Doctrine\ORM\EntityManager');

            $vote = new \Application\Entity\Votes();
            $vote->setNote_id($_POST['noteId']);
            $vote->setTab_id($_POST['tabId']);

            $note = WhoAmI::getNote($_POST['noteId']);
            $note->setVote($note->getVote() + 1);

            $objectManager->persist($vote);
            $objectManager->persist($note);
            $objectManager->flush();
            
            $cookie = new \Zend\Http\Header\SetCookie('votesavailable', $this->getRequest()->getHeaders()->get('Cookie')->votesavailable - 1, time() + 60 * 60 * 24 * 30, '/');
            $response = $this->getResponse()->getHeaders();
            $response->addHeader($cookie);
            
            $variables = array( 'ok' => TRUE, 'message' => 'Oddano głos na pomysł: ');
        }
        else{
            $variables = array( 'ok' => FALSE, 'message' => 'Liczba dostępnych głosów się skończyła, nie możesz już głosować!');
        }
        $json = new JsonModel( $variables );
        return $json;
    }
    function voteNumberGroupAction($tab_id, $id_group){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $votes = $objectManager->getRepository('\Application\Entity\Votes')
                ->findBy(
                    array('tab_id' => $tab_id),
                    array('groupe' => $id_group)   
                );
        $result = new JsonModel(array(
	    'number' => count($votes),
            'success'=>true
        ));
        return $result;
    }
}
