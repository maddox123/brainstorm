<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class GroupeController extends AbstractActionController{
    
    public function startGroupeAction(){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $links = $objectManager->getRepository('\Application\Entity\Links')
                ->findBy(array('id' => $this->getEvent()->getRouteMatch()->getParam('id')));
        $links[0]->setGroupe(1);
        
       $objectManager->persist($links[0]);
       $objectManager->flush();
       return false;
    }
    
    public function endGroupeAction(){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $links = $objectManager->getRepository('\Application\Entity\Links')
                ->findBy(array('id' => $this->getEvent()->getRouteMatch()->getParam('id')));
        $links[0]->setGroupe(0);
        
       $objectManager->persist($links[0]);
       $objectManager->flush();
       return false;
    }    
}
