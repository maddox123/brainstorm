<?php
/**
 * Description of WhoAmI
 *
 * @author Maciej_Wawryk
 */

namespace Application\Controller;

class WhoAmI {
    
    public function links($param){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $links = $objectManager->getRepository('\Application\Entity\Links')
                ->findBy(array('admin_link' => $param));
        
        if($links == NULL){
            $links = $objectManager->getRepository('\Application\Entity\Links')
                    ->findBy(array('user_link' => $this->getEvent()->getRouteMatch()->getParam('id')));
        }
        
        return $links;
    }
    
    public function tabId($param){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $links = $objectManager->getRepository('\Application\Entity\Links')
                ->findBy(array('admin_link' => $param));
        
        if($links == NULL){
            $links = $objectManager->getRepository('\Application\Entity\Links')
                    ->findBy(array('user_link' => $this->getEvent()->getRouteMatch()->getParam('id')));
        }
        
        return $links[0]->getId();
    }
    
    function admin($param, $links){
        if(strcmp($this->getEvent()->getRouteMatch()->getParam('id'), $links[0]->getAdmin_link()) == 0) return TRUE;
        else return FALSE;
    }
    function notes($id){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $notes = $objectManager->getRepository('\Application\Entity\Notes')
                ->findBy(
                    array('tab_id' => $id),
                    array('groupe' => 'DESC')
                );
        
        return $notes;
    }
    function groups($id){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $groups = $objectManager->getRepository('\Application\Entity\Groups')
                ->findBy(
                    array('tab_id' => $id)
                );
        
        return $groups;
    }
    function voteNumber($id){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $votes = $objectManager->getRepository('\Application\Entity\Votes')
                ->findBy(
                    array('tab_id' => $id)
                );
        
        return count($votes);
    }
    
    function getNote($id){
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $note = $objectManager->getRepository('\Application\Entity\Notes')
                ->findBy(
                    array('id' => $id)
                );
        
        return $note[0];
    }
    
    function dashboard_link($admin, $links){
        if($admin) return '/tab/index/'.$links[0]->getAdmin_link();
        else return '/tab/index/'.$links[0]->getUser_link();
    }
    
}
