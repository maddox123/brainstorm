<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class TabController extends AbstractActionController{
    public function indexAction(){

        if(!isset($this->getRequest()->getHeaders()->get('Cookie')->votesavailable)){
            $cookie = new \Zend\Http\Header\SetCookie('votesavailable', 5, time() + 60 * 60 * 24 * 30, '/');
            $response = $this->getResponse()->getHeaders();
            $response->addHeader($cookie);
        };
        
        $links = WhoAmI::links($this->getEvent()->getRouteMatch()->getParam('id'));
        $admin = WhoAmI::admin($this->getEvent()->getRouteMatch()->getParam('id'), $links);
        $notes = WhoAmI::notes($links[0]->getId());
        $link = WhoAmI::dashboard_link($admin, $links); 
        $votes = WhoAmI::voteNumber($links[0]->getId()); 
        $groups = WhoAmI::groups($links[0]->getId());
        if($votes == 0) $votes = 1;

        $layout = $this->layout();
        $layout->title = "Tablica: ".$links[0]->getName();
        $layout->userlink = $_SERVER['SERVER_NAME'].'/tab/index/'.$links[0]->getUser_link();
        $layout->admin = $admin;
        $layout->notes = $notes;
        $layout->votenumber = $votes;
        $layout->dashboard = $link;
        $layout->vote = $links[0]->getVote();
        $layout->tabId = $links[0]->getId();
        $layout->group = $links[0]->getGroupe();
        $layout->groups = $groups;
        $layout->setTemplate('layout/tab');
        
    }
    
    public function addNoteAction(){
        
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
       
        $note = new \Application\Entity\Notes();
        $note->setCreate_date(date("Y-m-d H:i:s"));
        $note->setTab_id($this->params()->fromPost('id'));
        $note->setName($this->params()->fromPost('idea'));
        $note->setDescription($this->params()->fromPost('description'));
        
        $objectManager->persist($note);
        $objectManager->flush();
        return FALSE;
        
    }
    public function editGroupeAction(){

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
       
        $note = WhoAmI::getNote($this->params()->fromPost('note_id'));
        $note->setGroup($this->params()->fromPost('number'));
        
        $objectManager->persist($note);
        $objectManager->flush();
        return FALSE;
    }
    public function addGroupeAction(){
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $groups = WhoAmI::groups($this->params()->fromPost('tab_id'));
        
        
        $groupe = new \Application\Entity\Groups();
        $groupe->setName($this->params()->fromPost('name'));
        if(!sizeof($groups))
        {
            $groupe->setGroupId(1);
        }
        else
        {
            $groupe->setGroupId($groups[sizeof($groups) - 1]->getId_Group()+1);
        }
        $groupe->setTab_id($this->params()->fromPost('tab_id'));
        
        $objectManager->persist($groupe);
        $objectManager->flush();
        
        $result = new JsonModel(array(
	    'groupe' => $groupe->getId_Group(),
            'success'=>true,
        ));

        return $result;       
    }
    public function addtabAction(){

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
       
       $links = new \Application\Entity\Links();
       $links->setAdmin_link(hash('ripemd160',date("Y-m-d H:i:s"). "admin"));
       $links->setUser_link(hash('ripemd160',date("Y-m-d H:i:s")));
       $links->setName("Burza mózgów #".rand());

       $objectManager->persist($links);
       $objectManager->flush();
       
       
       return $this->redirect()->toRoute('newtab', array('id'=>$links->getAdmin_link()));

    }
    
}
