<?php
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMysql\Driver',
                'params' => array(
                    'host' => 'localhost',
                    'user' => 'root',
                    'password' => '',
                    'dbname' => 'mydb',
                )
            )
        )
    ),
);
