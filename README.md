BrainstormApplication
=======================

Introduction
------------
This is a application using the ZF2 MVC layer and module systems. 

Installation
------------

Using Composer (recommended)
----------------------------
The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install dependencies using the `create-project` command:

    curl -s https://getcomposer.org/installer | php --
    php composer.phar create-project -sdev --repository-url="paste_here_repo_url" path/to/install

Alternately, clone the repository and manually invoke `composer` using the shipped
`composer.phar`:

    cd my/project/dir
    git clone git://repo_url.git
    cd ProjectName
    php composer.phar self-update
    php composer.phar install

(The `self-update` directive is to ensure you have an up-to-date `composer.phar`
available.)

If command php does'nt work for example php -v in command line (windows cmd) you habe to add path to php interpreter
My computer(Mój komputer)->Properties (Właściwości)->Advanced system sethings(Zaawansowane ustawienia systemu)->Environment Variables(Zmienne środowiskowe)
Double click PATH and add new path into the “variable value” - at the end ;C:\xampp\php (lokalizację interpretera php
After saving run another command line and test php -v

Web Server Setup
----------------

### PHP CLI Server

The simplest way to get started if you are using PHP 5.4 or above is to start the internal PHP cli-server in the root directory:

    php -S 0.0.0.0:8080 -t public/ public/index.php

This will start the cli-server on port 8080, and bind it to all network
interfaces.

**Note: ** The built-in CLI server is *for development only*.

### Apache Setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        ServerName zf2-tutorial.localhost
        DocumentRoot /path/to/zf2-tutorial/public
        SetEnv APPLICATION_ENV "development"
        <Directory /path/to/zf2-tutorial/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>


UPDATING:
php composer.phar install
call vendor\bin\doctrine-module orm:schema-tool:update --force